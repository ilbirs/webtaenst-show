package com.example.webservice1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Person {
    String id;
    String firstname;
    String lastname;
    List<Group> groupList;

    @JsonCreator
    public Person(@JsonProperty("id") String id,@JsonProperty("firstname") String firstname,@JsonProperty("lastname")
            String lastname,@JsonProperty("groupList") List<Group> groupList) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.groupList = groupList;
    }



    public void addGroup(Group group){
        groupList.add(group);
    }

}
