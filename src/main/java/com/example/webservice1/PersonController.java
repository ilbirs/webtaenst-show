package com.example.webservice1;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("persons")
@AllArgsConstructor
@CrossOrigin
public class PersonController {
    PersonService personService;

    @PostMapping("/create-person")
    public Person createPerson(@RequestParam String firstname, @RequestParam String lastname) {
        return personService.createPerson(firstname, lastname);
    }

    @GetMapping("/get-person-by-id/{id}")
    public Person getPersonById(@PathVariable String id) {
        return personService.getPersonById(id);
    }

    @GetMapping("/getAllPerson")
    public List<Person> getAllPerson(){
        return personService.getAllPersons().collect(Collectors.toList());
    }

    @PostMapping("/add-group-person")
    public Person addGroupPerson(@RequestParam String personId, @RequestParam String groupId) {
        return personService.addGroupPerson(personId, groupId);
    }

    @PutMapping("/update-person/{id}")
    public Person updatePersonById(@PathVariable String id, @RequestParam String firstname, @RequestParam String lastname) {
        return personService.updatePerson(id, firstname, lastname);
    }

    @DeleteMapping("/delete-person-by-id")
    public String deletePersonById(@RequestParam String id) throws PersonNotFoundException {
        return personService.deletePersonById(id);
    }

    @PostMapping("/create-group")
    public Group createGroup(@RequestParam String groupName) {
        return personService.createGroup(groupName);
    }

    @GetMapping("/get-all-group")
    public List<Group> getAllGroups() {
        return personService.getAllGroups();
    }
    @GetMapping("/get-all-persons")
    public List<Person> getAllPersons() {
        return personService.getAllPersons().collect(Collectors.toList());
    }

    @GetMapping("/get-groupName-by-id/{id}")
    public String getGroupName(@PathVariable String id) {
        return personService.getGroupName(id);
    }

    @PutMapping("/update-groupName/{id}")
    public Group updateGroupNameById(@PathVariable String id, @RequestParam String groupName) {
        return personService.updateGroup(id, groupName);
    }

    @DeleteMapping("/delete-group-by-id")
    public String deleteGroupById(@RequestParam String id) {
        return personService.deleteGroupById(id);
    }

}
