package com.example.webservice1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Group {
     String id;
     String groupName;
     List<String> members;

     @JsonCreator
     public Group(@JsonProperty("id") String id,
                  @JsonProperty("groupName") String groupName,
                  @JsonProperty("members") List<String> members) {
          this.id = id;
          this.groupName = groupName;
          this.members = members;
     }
}
/*jgfdfvsfvsdxvsfdvsfdfsfgssdjygjhj*/
